using UnityEngine;

public class ArmShotgun : ArmBase
{
    [SerializeField]
    int bulletsPerShot = 13;

    [SerializeField]
    float maxAngle = 15;
    
    public override void Shoot()
    {
        for (int i = 0; i < bulletsPerShot; i++)
        {
            Bullet bullet = BulletPool.Get();
            bullet.Setup(player != null, ShootPoint.position, ShootPoint.rotation, maxAngle);
        }
        
        base.Shoot();
    }
    
    public override ArmType Type { get { return ArmType.Shotgun; } }
}
