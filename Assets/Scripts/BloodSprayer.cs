using UnityEngine;

public class BloodSprayer : MonoBehaviour
{
    [SerializeField]
    bool isPlayer = false;

    [SerializeField]
    int maxDecalCount = 10;

    [SerializeField]
    float decalInterval = 0.5f;

    [SerializeField]
    LayerMask hitLayers = 0;

    int decalCount = 0;
    float timer = 0;

    void OnEnable()
    {
        decalCount = 0;
        timer = 0;
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer > decalInterval)
        {
            if (isPlayer)
            {
                GameManager.instance.SpawnBloodToPlayerSides();
                decalCount++;
            }
            else
            {
                Ray ray = new Ray(transform.position, transform.forward);
                if (Physics.Raycast(ray, out RaycastHit hit, 8f, hitLayers))
                {
                    GameManager.instance.SpawnBlood(hit.point);
                    decalCount++;
                }
            }

            timer = 0;
        }

        if (decalCount >= maxDecalCount)
        {
            gameObject.SetActive(false);
        }
    }
}
