using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    EnemyManager enemyManager = null;

    [SerializeField]
    SettingsSO settings = null;

    [SerializeField]
    PrefabsSO prefabs = null;
    
    [SerializeField]
    AudioClip[] enemyAudioClips = null;

    [SerializeField]
    float enemyAudioInterval = 10f;
    
    [SerializeField]
    AudioClip[] bloodSplatterAudioClips = null;

    [SerializeField]
    float bloodSplatterAudioInterval = 0.5f;
    
    [SerializeField]
    AudioClip[] announcerAudioClips = null;

    [SerializeField]
    AudioSource announcerAudioSource = null;

    ObjectPool<Bullet> bulletPool = null;

    RaycastTarget raycastTarget = null;
    CameraFollow cameraFollow = null;

    Transform bulletPoolParent = null;
    Transform bloodDecalParent = null;
    Transform mousePos = null;
    

    AudioSource playerHitSound;

    int playerHp = 5;
    float lastDamage = 0;
    float bloodSplatterAudioTimer = 0f;

    List<AudioSource> bloodSplatterAudioSources = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            DestroyImmediate(this);
    }

    void OnDestroy()
    {
        bloodSplatterAudioSources.Clear();
        bloodSplatterAudioSources = null;
        
        cameraFollow.Dispose();
        Destroy(Camera);
        Camera = null;
        cameraFollow = null;
        
        raycastTarget.Dispose();
        Destroy(raycastTarget);
        
        Player.Dispose();
        Destroy(Player);
        Player = null;
        
        bulletPool.Dispose();
        bulletPool = null;
        Destroy(bulletPoolParent);
        
        Destroy(bloodDecalParent);
        
        ArmManager.Dispose();
        enemyManager.Dispose();
        ArmManager = null;
    }

    void Start()
    {
        InstantiateStuff();
        SetupStuff();
        
    }

    void InstantiateStuff()
    {
        bulletPoolParent = new GameObject("BulletPool").transform;
        bloodDecalParent = new GameObject("BloodDecals").transform;
        mousePos = new GameObject("FollowPoint").GetComponent<Transform>();

        Player = Instantiate(prefabs.Player);
        Camera = Instantiate(prefabs.Camera);
        raycastTarget = Instantiate(prefabs.RaycastTarget);
        ArmManager = new ArmManager();

        bloodSplatterAudioSources = new List<AudioSource>(16);
        for(int i = 0; i < 16; i++)
        {
            GameObject audioSourceObj = new GameObject("BloodSplatter audio source");
            audioSourceObj.transform.parent = transform;
            AudioSource audioSource = audioSourceObj.AddComponent<AudioSource>();
            audioSource.loop = false;
            audioSource.playOnAwake = false;
            audioSource.spatialBlend = 0.5f;
            bloodSplatterAudioSources.Add(audioSource);
        }

        {
            GameObject audioSourceObj = new GameObject("BloodSplatter audio source");
            audioSourceObj.transform.parent = transform;
            AudioSource audioSource = audioSourceObj.AddComponent<AudioSource>();
            audioSource.loop = false;
            audioSource.playOnAwake = false;
            audioSource.spatialBlend = 0.5f;
            bloodSplatterAudioSources.Add(audioSource);

            playerHitSound = audioSource;
        }
    }

    void SetupStuff()
    {
        bulletPool = new ObjectPool<Bullet>();
        bulletPool.Setup(prefabs.Bullet, 256, bulletPoolParent);
        
        Player.Setup(ArmManager, Camera);
        ArmManager.Setup(Player.transform, settings.PickupDistance);
        ArmBase arm = GetArm(ArmType.Pistol);
        Player.EquipArm(arm);
        //arm = GetRandomArm();
        //Player.EquipArm(arm);

        raycastTarget.Setup(Player.transform);
        
        cameraFollow = Camera.GetComponent<CameraFollow>();
        cameraFollow.Setup(mousePos);
        //cameraFollow.Setup(Player.transform);

        enemyManager = new EnemyManager();
        enemyManager.Setup(prefabs);
    }
    
    void Update()
    {
        bloodSplatterAudioTimer -= Time.deltaTime;

        if (PlayerHp <= 0) return;
        
        ArmManager._Update();
        Player._Update();
        enemyManager.Update();
        
        if(Input.GetButtonUp("Cancel"))
        {
            SceneManager.LoadScene(0);
        }
    }

    void LateUpdate()
    {
        raycastTarget._Update();
        cameraFollow._Update();
        
        var mPos = Input.mousePosition;
        mPos.x -= Screen.width / 2f;
        mPos.z = mPos.y - Screen.height / 2f;
        mPos.y = 0;

        mPos = mPos / new Vector3(Screen.width/2f, 0, Screen.height/2f).magnitude * 7;
        mPos = Vector3.ClampMagnitude(mPos, 2);
        
        mousePos.transform.position = Vector3.Lerp(mousePos.transform.position, Player.transform.position + mPos, Time.deltaTime * 4);
    }

    void FixedUpdate()
    {
        if (PlayerHp <= 0) return;
        Player._FixedUpdate();
        enemyManager._FixedUpdate();
    }

    public ArmBase GetArm(ArmType type)
    {
        for (int i = 0; i < prefabs.Arms.Length; i++)
        {
            if (prefabs.Arms[i].Type == type)
            {
                return GetArm(i);
            }
        }

        Debug.LogError($"Couldn't find arm with type {type}! Check Prefabs scriptable object");
        return null;
    }
    
    public ArmBase GetRandomArm()
    {
        int rng = Random.Range(0, prefabs.Arms.Length);
        return GetArm(rng);
    }

    ArmBase GetArm(int index)
    {
        ArmBase arm = Instantiate(prefabs.Arms[index].Prefab);
        arm.Setup(ArmManager, BulletPool);
        return arm;
    }
    
    public void TakeDamage()
    {
        if (lastDamage + settings.InvulnerabilityTimer < Time.time)
        {

            playerHitSound.clip = settings.DamageSound;
            playerHitSound.Play();

            StartCoroutine(VignetteAnimation());
            
            lastDamage = Time.time;
            playerHp = PlayerHp - 1;
            IngameHud.Instance.UpdateHealthValue(PlayerHp/5f);
            if (PlayerHp <= 0)
            {
                IngameHud.Instance.ShowEndScreen();
                
                var time = ToikkaTimerScript.Instance.GameTime;
                bool newBestTime = false;

                float bestTime = PlayerPrefs.GetFloat("BestTime", 0);
                if (time > bestTime)
                {
                    PlayerPrefs.SetFloat("BestTime", time);
                    PlayerPrefs.Save();
                    newBestTime = true;
                }

                IngameHud.Instance.BestTime(newBestTime, Mathf.Max(time,bestTime));

            }

        }
    }

    IEnumerator VignetteAnimation()
    {
        Vignette vignette = Camera.GetComponent<Volume>().profile.components[13] as Vignette;

        float startTime = Time.time;
        float duration = 0.2f;

        while (startTime + duration > Time.time)
        {
            vignette.intensity.value = Mathf.Lerp(0f, 0.6f,(Time.time - startTime) / duration);
            yield return null;
        }
        
        startTime = Time.time;
        
        while (startTime + duration > Time.time)
        {
            vignette.intensity.value = Mathf.Lerp(0.6f, 0f,(Time.time - startTime) / duration);
            yield return null;
        }
    }

    public void SpawnBloodToPlayerSides()
    {
        Vector3 playerPos = Player.transform.position;
        playerPos.x += Random.Range(-2f, 2f);
        playerPos.z += Random.Range(-0.5f, 0.5f);
        SpawnBlood(playerPos);
    }
    
    public void SpawnBlood(Vector3 position)
    {
        position.y = 0.2f;
        DecalProjector blood = Instantiate(prefabs.BloodDecals[Random.Range(0, prefabs.BloodDecals.Length)], bloodDecalParent);
        blood.transform.position = position;
        blood.transform.RotateAround(position,Vector3.up,Random.Range(0,360));
        int size = Random.Range(3, 10);
        blood.size = new Vector3(size,size,3);

        if (bloodSplatterAudioTimer < 0)
        {
            bloodSplatterAudioTimer = bloodSplatterAudioInterval;
            foreach (AudioSource audioSource in bloodSplatterAudioSources)
            {
                if (audioSource.isPlaying) continue;
                
                audioSource.transform.position = position;
                audioSource.clip = bloodSplatterAudioClips[Random.Range(0, bloodSplatterAudioClips.Length)];
                audioSource.Play();
                break;
            }
        }
    }

    public void WaveStarted()
    {
        announcerAudioSource.clip = announcerAudioClips[Random.Range(0, announcerAudioClips.Length)];
        announcerAudioSource.Play();
    }

    public PrefabsSO Prefabs { get { return prefabs; } }
    public ObjectPool<Bullet> BulletPool { get { return bulletPool; } }
    public EnemyManager EnemyManager { get { return enemyManager; } }
    
    public Player Player { get; private set; }
    public Camera Camera { get; private set; }
    
    public ArmManager ArmManager { get; private set; }
    public AudioClip[] EnemyAudioClips { get { return enemyAudioClips; } }
    public float EnemyAudioInterval { get { return enemyAudioInterval; } }
    public int PlayerHp { get { return playerHp; } }
}
