using UnityEngine;
using Random = UnityEngine.Random;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    Rigidbody rb = null;

    [SerializeField]
    float speed = 50f;

    [SerializeField]
    float aliveTimer = 2.5f;

    float timer = 0;

    bool playerShot;
    int layerMask;
    int layerMaskDefault;

    void Awake()
    {
        gameObject.SetActive(false);
    }

    public void Setup(bool playerBullet, Vector3 pos, Quaternion rot, float randomAngle = -1f)
    {
        playerShot = playerBullet;
        if (playerBullet)
        {
            layerMask = LayerMask.GetMask("Enemies", "Default");
        }
        else
        {
            layerMask = LayerMask.GetMask("Player", "Default");
        }

        layerMaskDefault = LayerMask.GetMask("Default");

        Transform t = transform;
        t.position = pos;
        t.rotation = rot;
        if (randomAngle > 0)
        {
            float angleXOffset = Random.Range(-randomAngle, randomAngle);
            float angleYOffset = Random.Range(-randomAngle, randomAngle);
            t.Rotate(angleXOffset, angleYOffset, 0);
        }

        Activate();
    }

    void Activate()
    {
        timer = 0;
        gameObject.SetActive(true);
        rb.velocity = transform.forward * speed;
    }
    
    void Dispose()
    {
        timer = 0;
        gameObject.SetActive(false);
        GameManager.instance.BulletPool.Return(this);
    }

    void Update()
    {
        Ray ray = new Ray(transform.position,transform.forward);
        bool hit = Physics.Raycast(ray, out RaycastHit hitInfo, 0.25f, layerMask);

        if (hit)
        {
            if (playerShot)
            {
                EnemyBase enemy = hitInfo.transform.GetComponent<EnemyBase>();
                if (enemy != null)
                {
                    enemy.Die();

                    ray = new Ray(hitInfo.point, transform.forward);
                    if (Physics.Raycast(ray, out RaycastHit hitInfo2, 100f, layerMaskDefault))
                    {
                        GameManager.instance.SpawnBlood(hitInfo2.point);
                    }
                }
            }
            else
            {
                Player player = hitInfo.transform.GetComponent<Player>();
                if (player != null)
                {
                    GameManager.instance.TakeDamage();
                }
            }

            Dispose();
        }

        timer += Time.deltaTime;
        if (timer > aliveTimer)
        {
            Dispose();
        }
    }
}
