using System.Collections.Generic;
using UnityEngine;

public class ArmManager
{
    Transform player = null;
    float pickupDistance = 5f;
    ArmBase closestArm = null;

    List<ArmBase> arms = null;

    public void Setup(Transform player, float pickupDistance)
    {
        this.player = player;
        this.pickupDistance = pickupDistance;
        arms = new List<ArmBase>();
    }

    public void Dispose()
    {
        player = null;
        
        arms.Clear();
        arms = null;
    }

    public void _Update()
    {
        closestArm = null;
        float closestDistanceSqr = float.PositiveInfinity;
        Vector3 playerPos = player.transform.position;
        float pickupDistanceSqr = pickupDistance * pickupDistance;
        foreach (ArmBase arm in arms)
        {
            float distanceSqr = (playerPos - arm.transform.position).sqrMagnitude;
            if (distanceSqr < closestDistanceSqr && distanceSqr < pickupDistanceSqr)
            {
                closestDistanceSqr = distanceSqr;
                closestArm = arm;
            }
        }

        /*if (closestArm != null)
        {
            GameManager.instance.Player.EquipArm(closestArm);
            PickupArm();
        }*/
    }

    public void AddArm(ArmBase arm)
    {
        if (arms.Contains(arm))
        {
            Debug.LogError($"Arm already registered!");
            return;
        }
        
        arms.Add(arm);
    }

    public void RemoveArm(ArmBase arm)
    {
        arms.Remove(arm);
    }

    public ArmBase PickupArm()
    {
        ArmBase arm = closestArm;
        closestArm = null;
        RemoveArm(arm);
        return arm;
    }

    public ArmBase ClosestArm { get { return closestArm; } }
}
