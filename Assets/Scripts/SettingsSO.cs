using UnityEngine;

[CreateAssetMenu(fileName = "Settings", menuName = "ScriptableObjects/Settings")]
public class SettingsSO : ScriptableObject
{
    [SerializeField]
    float pickupDistance = 5f;
    [SerializeField]
    float invulnerabilityTimer = 1f;

    [SerializeField]
    AudioClip damageSound;

    public float PickupDistance { get { return pickupDistance; } }
    public float InvulnerabilityTimer { get { return invulnerabilityTimer; } }
    public AudioClip DamageSound { get { return damageSound; } }
}