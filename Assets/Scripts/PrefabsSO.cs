using System;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;

[CreateAssetMenu(fileName = "PrefabsData", menuName = "ScriptableObjects/PrefabsData")]
public class PrefabsSO : ScriptableObject
{
    [SerializeField]
    Camera camera = null;
    
    [SerializeField]
    Player player = null;
    
    [SerializeField]
    EnemyBase[] enemies = null;

    [SerializeField]
    int[] waves;
    
    [SerializeField]
    Vector3[] enemySpawnPositions = null;

    [SerializeField]
    DecalProjector[] bloodDecals = null;

    [SerializeField]
    RaycastTarget raycastTarget = null;

    [SerializeField]
    Bullet bullet = null;

    [SerializeField]
    ArmData[] arms = null;

    public Camera Camera { get { return camera; } }
    public Player Player { get { return player; } }
    public EnemyBase[] Enemies { get { return enemies; } }
    public int[] Waves { get { return waves; } }
    public Vector3[] EnemySpawnPositions { get { return enemySpawnPositions; } }
    public DecalProjector[] BloodDecals { get { return bloodDecals; } }
    public RaycastTarget RaycastTarget { get { return raycastTarget; } }
    public Bullet Bullet { get { return bullet; } }
    public ArmData[] Arms { get { return arms; } }

}

[Serializable]
public class ArmData
{
    [SerializeField]
    ArmType type = ArmType.None;

    [SerializeField]
    ArmBase prefab = null;

    public ArmType Type { get { return type; } }
    public ArmBase Prefab { get { return prefab; } }
}