using System;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class EnemyBase : MonoBehaviour
{
    [SerializeField]
    float targetDistanceMin = 10;
    
    [SerializeField]
    float targetDistanceMax = 30;

    [SerializeField]
    float attackInterval = 5;

    [SerializeField]
    float moveSpeed = 3;

    [SerializeField]
    Transform weaponPoint;

    [SerializeField]
    GameObject[] disableOnDeath = null;
    
    [SerializeField]
    GameObject[] enableOnDeath = null;

    [SerializeField]
    Animator animator = null;

    [SerializeField]
    Transform lookAtPart = null;

    [SerializeField]
    AudioSource audioSource = null;
    
    static readonly int RUN_BACKWARDS = Animator.StringToHash("RunBackwards");
    static readonly int RUN = Animator.StringToHash("Run");
    
    protected ArmBase weapon;
    protected GameManager manager;
    
    Player player;
    Rigidbody rb;
    float attackTime = 0;
    float stopShootingTimer = 0;
    
    bool dead = false;

    NavMeshAgent navMeshAgent = null;

    public virtual void Setup(GameManager manager)
    {
        this.manager = manager;
        rb = GetComponent<Rigidbody>();
        attackTime = attackInterval + Random.Range(0, 0.5f);
        
        weapon = manager.GetRandomArm();
        weapon.SetAsEnemyWeapon();
        Transform weaponTransform = weapon.transform;
        weaponTransform.SetParent(weaponPoint, false);
        weaponTransform.localPosition = Vector3.zero;
        weaponTransform.localRotation = Quaternion.identity;

        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.avoidancePriority = Random.Range(1, 10000);
        navMeshAgent.speed = moveSpeed;
    }
    
    public void _Update()
    {
        if (dead)
        {
            return;
        }

        if (weapon.IsShooting)
        {
            stopShootingTimer += Time.deltaTime;
            if (stopShootingTimer > 0.25f)
            {
                stopShootingTimer = 0;
                weapon.ShootEnd();
            }
        }
        else
        {
            attackTime -= Time.deltaTime;
            if (attackTime < 0)
            {
                weapon.Shoot();
                attackTime = attackInterval + Random.Range(0, 0.5f);
            }
        }
        
        Vector3 playerPosition = manager.Player.transform.position;
        Vector3 position = transform.position;
        float distance = Vector3.Distance(position, playerPosition);
        
        lookAtPart.LookAt(playerPosition);

        if (distance > targetDistanceMax)
        {
            navMeshAgent.enabled = true;
            
            if (!isMoving)
            {
                AgentMove(playerPosition);
            }
        }

        if (distance < targetDistanceMin)
        {
            navMeshAgent.enabled = false;
            isMoving = false;
            animator.SetBool(RUN, false);
            rb.velocity = Vector3.zero;
        }
    }

    bool isMoving = false;
    void AgentMove(Vector3 playerPosition)
    {
        navMeshAgent.destination = playerPosition;
        animator.SetBool(RUN, true);
    }
    
    public void _FixedUpdate()
    {
        /*Vector3 playerPosition = manager.Player.transform.position;
        Vector3 position = transform.position;
        float distance = Vector3.Distance(position, playerPosition);
        //Vector3 direction = playerPosition - position;
        
        Vector3 hitPos = playerPosition;
        hitPos.y = position.y;
        transform.LookAt(hitPos);
        
        if (distance < targetDistanceMin)
        {
            rb.velocity = transform.forward * -moveSpeed;
            
        }
        else if (distance > targetDistanceMax)
        {
            rb.velocity = transform.forward * moveSpeed;
        }
        else
        {
            //some lateral movement
        }
        
        
        rotatePart.LookAt(transform.position + rb.velocity);
        Vector3 relativeMoveDir = transform.InverseTransformDirection(rb.velocity);
        float z = relativeMoveDir.z;

        if (z >= 0f)
        {
            animator.SetBool(RUN, true);
            animator.SetBool(RUN_BACKWARDS, false);
        }
        else
        {
            rotatePart.Rotate(0, 180, 0);
            animator.SetBool(RUN, false);
            animator.SetBool(RUN_BACKWARDS, true);
        }*/
    }

    public void Die()
    {
        if (dead)
        {
            return;
        }
        
        navMeshAgent.enabled = false;
        animator.SetBool(RUN, false);
        
        GameManager.instance.SpawnBlood(transform.position);
        
        dead = true;
        manager.EnemyManager.RemoveDeadEnemy(this);
        weapon.Drop();
        weapon = null;

        foreach (GameObject go in disableOnDeath)
        {
            go.SetActive(false);
        }

        foreach (GameObject go in enableOnDeath)
        {
            go.SetActive(true);
        }

        rb.useGravity = true;
        rb.constraints = RigidbodyConstraints.None;
    }

    public AudioSource AudioSource { get { return audioSource; } }
}
