using UnityEngine;

public class RaycastTarget : MonoBehaviour
{
    Transform followTarget = null;

    public void Setup(Transform followTarget)
    {
        this.followTarget = followTarget;
    }

    public void Dispose()
    {
        followTarget = null;
    }
    
    public void _Update()
    {
        Vector3 pos = followTarget.position;
        Transform t = transform;
        pos.y = -t.localScale.y / 2f;
        t.position = pos;
    }
}
