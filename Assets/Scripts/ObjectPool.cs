using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

[Serializable]
public class ObjectPool<T> where T : Object
{
    class PoolEntry
    {
        public PoolEntry(T obj)
        {
            Obj = obj;
            IsActive = false;
        }

        public T Obj { get; }
        public bool IsActive { get; set; }
    }
    
    T prefab = default;
    List<PoolEntry> pool = null;
    int currentIndex = 0;
    Transform parent = null;

    public void Setup(T prefab, int initialCount, Transform parent)
    {
        this.prefab = prefab;
        this.parent = parent;
        pool = new List<PoolEntry>(initialCount);
        for (int i = 0; i < initialCount; i++)
        {
            AddEntry();
        }
    }

    public virtual void Dispose()
    {
        pool.Clear();
        pool = null;
    }

    PoolEntry AddEntry()
    {
        T item = Object.Instantiate(prefab, parent);
        PoolEntry poolEntry = new PoolEntry(item);
        pool.Add(poolEntry);
        poolEntry.IsActive = false;
        return poolEntry;
    }

    public T Get()
    {
        PoolEntry entry;
        for (int i = 0; i < pool.Count; i++)
        {
            currentIndex++;
            if (currentIndex >= pool.Count)
            {
                currentIndex = 0;
            }

            entry = pool[currentIndex];
            if (!entry.IsActive)
            {
                entry.IsActive = true;
                return entry.Obj;
            }
        }

        entry = AddEntry();
        entry.IsActive = true;
        return entry.Obj;
    }

    public void Return(T obj)
    {
        foreach (PoolEntry poolEntry in pool)
        {
            if (poolEntry.Obj == obj)
            {
                poolEntry.IsActive = false;
                return;
            }
        }

        Object.Destroy(obj);
    }
}
