using System.Collections.Generic;
using UnityEngine;

public class EnemyManager
{
    int waveCount = 0;

    GameManager manager;
    Player player;
    PrefabsSO prefabs;

    Transform enemyRoot;
    List<EnemyBase> activeEnemies;

    float audioTimer = 0;

    public void Setup(PrefabsSO prefabs)
    {
        manager = GameManager.instance;
        player = manager.Player;
        this.prefabs = prefabs;
        
        enemyRoot = new GameObject("EnemyRoot").transform;
        enemyRoot.position = Vector3.zero;
        activeEnemies = new List<EnemyBase>();

        audioTimer = manager.EnemyAudioInterval - 3f;
    }
    
    public void Dispose()
    {
        manager = null;
        prefabs = null;
        player = null;
    }

    public void Update()
    {
        if (activeEnemies.Count == 0)
        {
            StartNewWave();
        }
        
        foreach (EnemyBase enemy in activeEnemies)
        {
            enemy._Update();
        }

        audioTimer += Time.deltaTime;
        if (activeEnemies.Count > 0 && audioTimer > manager.EnemyAudioInterval)
        {
            audioTimer = Random.Range(-2f, 2f);
            EnemyBase enemy = activeEnemies[Random.Range(0, activeEnemies.Count)];
            enemy.AudioSource.clip = manager.EnemyAudioClips[Random.Range(0, manager.EnemyAudioClips.Length)];
            enemy.AudioSource.Play();
        }
    }
    
    public void _FixedUpdate()
    {
        foreach (EnemyBase enemy in activeEnemies)
        {
            enemy._FixedUpdate();
        }
    }

    void StartNewWave()
    {
        int differentEnemies = prefabs.Enemies.Length - 1;
        int[] waves = manager.Prefabs.Waves;
        Transform[] spawnPoints = SpawnPoints.instance.Points;
        
        if (waves.Length > waveCount)
        {
            for (int i = 0; i < waves[waveCount]; i++)
            {
                EnemyBase newEnemy = GameObject.Instantiate(prefabs.Enemies[Random.Range(0, differentEnemies)], enemyRoot);
                newEnemy.transform.position = spawnPoints[Random.Range(0, spawnPoints.Length)].position;
                newEnemy.Setup(manager);
                activeEnemies.Add(newEnemy);
            }
        }
        else
        {
            //var randomWave = Random.Range(waves.Length / 2, waves.Length);
            for (int i = 0; i < Mathf.Max(30, waveCount); i++)
            {
                EnemyBase newEnemy = GameObject.Instantiate(prefabs.Enemies[Random.Range(0, differentEnemies)], enemyRoot);
                newEnemy.transform.position = spawnPoints[Random.Range(0, spawnPoints.Length)].position;
                newEnemy.Setup(manager);
                activeEnemies.Add(newEnemy);
            }
        }

        waveCount++;
        IngameHud.Instance.UpdateWaveNumber(waveCount);
        
        if (waveCount > 1)
        {
            manager.WaveStarted();
        }
    }

    public void RemoveDeadEnemy(EnemyBase enemy)
    {
        activeEnemies.Remove(enemy);
    }
}
