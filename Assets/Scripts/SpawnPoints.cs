using System;
using UnityEngine;

public class SpawnPoints : MonoBehaviour
{
    [SerializeField]
    Transform[] spawnPoints = null;
    
    public static SpawnPoints instance;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            DestroyImmediate(this);
    }

    public Transform[] Points { get { return spawnPoints; } }
}
