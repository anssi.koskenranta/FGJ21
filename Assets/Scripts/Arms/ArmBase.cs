using UnityEngine;

public abstract class ArmBase : MonoBehaviour
{
    [SerializeField]
    int maxUsages = 10;

    [SerializeField]
    Transform shootPoint = null;

    [SerializeField]
    AudioClip[] audioClips = null;

    [SerializeField]
    AudioSource audioSource = null;
    
    protected bool playerArm = true;

    [SerializeField]
    Rigidbody rb = null;

    [SerializeField]
    GameObject highLight = null;

    ArmManager armManager = null;
    protected Player player = null;
    bool isleftArm = true;

    protected bool isShooting = false;

    public virtual void Setup(ArmManager armManager, ObjectPool<Bullet> bulletPool)
    {
        this.armManager = armManager;
        BulletPool = bulletPool;
        Ammo = MaxAmmo;
    }

    public virtual void Dispose()
    {
        armManager.RemoveArm(this);
        BulletPool = null;
    }

    public virtual void Shoot()
    {
        if (AudioClips.Length > 0)
        {
            AudioClip clip = AudioClips[Random.Range(0, AudioClips.Length)];
            AudioSource.PlayOneShot(clip);
        }
        
        if (playerArm)
        {
            Ammo--;
            IngameHud.Instance.UpdateArmInfo(isleftArm, Ammo, Type.ToString().ToUpper());
            
            if (Ammo <= 0)
            {
                Drop();
            }
        }
    }

    public virtual void ShootEnd() { }

    public virtual void Drop()
    {
        isShooting = false;
        
        transform.SetParent(null);
        if (player != null)
        {
            player.RemoveArm(this);
            player = null;
        }
        else
        {
            armManager.AddArm(this);
            highLight.SetActive(true);
        }
        rb.constraints = RigidbodyConstraints.None;
    }

    public virtual void Equip(Player player, Transform shoulder, bool leftArm)
    {
        isleftArm = leftArm;
        playerArm = true;
        this.player = player;
        Transform t = transform;
        t.parent = shoulder;
        t.localPosition = Vector3.zero;
        t.localRotation = Quaternion.identity;
        t.localScale = Vector3.one;
        rb.constraints = RigidbodyConstraints.FreezeAll;
        highLight.SetActive(false);
    }

    public void SetAsEnemyWeapon()
    {
        playerArm = false;
    }

    public abstract ArmType Type { get; }
    protected ObjectPool<Bullet> BulletPool { get; private set; }
    public int MaxAmmo { get { return maxUsages; } }
    public int Ammo { get; protected set; }
    protected Transform ShootPoint { get { return shootPoint; } }
    public AudioClip[] AudioClips { get { return audioClips; } }
    public AudioSource AudioSource { get { return audioSource; } }
    public bool IsShooting { get { return isShooting; } }
}
