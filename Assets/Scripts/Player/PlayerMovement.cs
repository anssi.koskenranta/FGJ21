using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    Animator animator = null;

    [SerializeField]
    Transform rotatePart = null;

    static readonly int RUN = Animator.StringToHash("Run");
    static readonly int RUN_BACKWARDS = Animator.StringToHash("RunBackwards");
    
    Player player = null;
    Rigidbody rb = null;

    public void Setup(Player player)
    {
        this.player = player;
        rb = GetComponent<Rigidbody>();
    }

    public void Dispose()
    {
        player = null;
        rb = null;
    }
    
    public void _FixedUpdate()
    {
        float moveV = Input.GetAxisRaw("Vertical");
        float moveH = Input.GetAxisRaw("Horizontal");

        Vector3 move = new Vector3 (moveH, 0f, moveV).normalized;
        rb.velocity = move * player.MoveSpeed;

        if (move.sqrMagnitude < 0.5f)
        {
            animator.SetBool(RUN, false);
            animator.SetBool(RUN_BACKWARDS, false);
        }
        else
        {
            Vector3 relativeMoveDir = transform.InverseTransformDirection(move);
            
            rotatePart.LookAt(transform.position + move);
            float z = relativeMoveDir.z;

            if (z >= 0f)
            {
                animator.SetBool(RUN, true);
                animator.SetBool(RUN_BACKWARDS, false);
            }
            else
            {
                rotatePart.Rotate(0, 180, 0);
                animator.SetBool(RUN, false);
                animator.SetBool(RUN_BACKWARDS, true);
            }
        }
    }
}
