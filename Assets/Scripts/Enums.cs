public enum ArmType
{
    None = 0,
    
    //Projectile
    Pistol = 1,
    Shotgun = 2,
    Minigun = 3,
    
    //Explosive
    RocketLauncher = 100,
    
    //Melee
    Chainsaw = 200,
    
    //Others
    Shield = 300,
}