using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class IngameHud : MonoBehaviour
{
    [SerializeField]
    public Image healthBar;

    [SerializeField]
    TMP_Text waveCounter;

    [SerializeField]
    GameObject pauseMenu;

    [SerializeField]
    ArmChargeController armChargeController;

    [SerializeField]
    GameObject pickupArmPopup = null;

    [SerializeField]
    GameObject endScreen = null;
    
    [SerializeField]
    GameObject bestTime = null;

    [SerializeField]
    TMP_Text bestTimeText = null;

    [SerializeField]
    float endTime = 2;

    public static IngameHud Instance;

    float preventInputTime = 0;
    bool endGame = false;

    public void Awake()
    {
        Instance = this;
        //pauseMenu.SetActive(false);
    }

    void Update()
    {
        if (!endGame)
        {
            if (GameManager.instance.ArmManager.ClosestArm != null && GameManager.instance.Player.CanPickupArm)
            {
                if (!pickupArmPopup.activeSelf)
                    pickupArmPopup.SetActive(true);
            }
            else
            {
                if (pickupArmPopup.activeSelf)
                    pickupArmPopup.SetActive(false);
            }    
        }
        else
        {
            if (preventInputTime + endTime > Time.time) return;

            if (Input.GetButtonUp("Pickup"))
            {
                PlayAgain();
            }    
            if(Input.GetButtonUp("Cancel"))
            {
                QuitGame();
            }
        }
    }

    public void PauseMenu(bool value)
    {
        pauseMenu.SetActive(value);
    }

    public void UpdateWaveNumber(int waveCount)
    {
        waveCounter.SetText(waveCount.ToString()); 
    }

    public void UpdateHealthValue(float health)
    {
        healthBar.fillAmount = health;
    }

    public void UpdateArmInfo(bool leftArm, int ammo, string weaponName)
    {
        armChargeController.UpdateValue(leftArm, ammo, weaponName);
    }

    public void ShowEndScreen()
    {
        preventInputTime = Time.time;
        
        pickupArmPopup.SetActive(false);
        endScreen.SetActive(true);
        endGame = true;
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        SceneManager.LoadScene(0);
    }

    public void BestTime(bool newBestTime, float time)
    {
        bestTime.gameObject.SetActive(newBestTime);
        
        float t_minutes = ((int)time / 60); 
        float t_seconds = ((int)time % 60); 
        float t_milliseconds = ((int)(time * 100)) % 100; 
        
        bestTimeText.text = string.Format("{0:00}:{1:00}:{2:00}", t_minutes, t_seconds, t_milliseconds);
    }
}
