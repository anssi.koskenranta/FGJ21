using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField]
    float moveSpeed = 10f;
    
    [SerializeField]
    PlayerMovement playerMovement = null;

    [Header("Aiming")]
    [SerializeField]
    LayerMask raycastLayer = 0;

    [Header("Arms")]
    [SerializeField]
    Transform shoulderLeft = null;
    
    [SerializeField]
    Transform shoulderRight = null;
    
    [SerializeField]
    AudioClip[] lostArmAudioClips = null;
    
    [SerializeField]
    AudioClip[] killedEnemyAudioClips = null;
    
    [SerializeField]
    AudioClip[] pickupArmAudioClips = null;

    [SerializeField]
    AudioSource audioSource = null;

    [SerializeField]
    float audioCooldown = 8f;

    [SerializeField]
    GameObject bloodSprayerLeft = null;
    
    [SerializeField]
    GameObject bloodSprayerRight = null;

    ArmManager armManager = null;
    new Camera camera = null;
    
    ArmBase leftArm = null;
    ArmBase rightArm = null;

    float audioCooldownTimer = 0;

    public void Setup(ArmManager armManager, Camera camera)
    {
        this.armManager = armManager;
        this.camera = camera;
        playerMovement.Setup(this);
    }

    public void Dispose()
    {
        playerMovement.Dispose();
    }

    public void _Update()
    {
        if (leftArm != null)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                leftArm.Shoot();
            }
            else if (Input.GetButtonUp("Fire1"))
            {
                leftArm.ShootEnd();
            }
        }

        if (rightArm != null)
        {
            if (Input.GetButtonDown("Fire2"))
            {
                rightArm.Shoot();
            }
            else if (Input.GetButtonUp("Fire2"))
            {
                rightArm.ShootEnd();
            }
        }

        if (Input.GetButtonUp("Pickup"))
        {
            if (armManager.ClosestArm != null && CanPickupArm)
            {
                ArmBase arm = armManager.PickupArm();
                EquipArm(arm);
            }
        }

        audioCooldownTimer -= Time.deltaTime;
    }

    public void _FixedUpdate()
    {
        playerMovement._FixedUpdate();
        
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit hit, maxDistance: 100f, raycastLayer))
        {
            Vector3 hitPos = hit.point;
            hitPos.y = transform.position.y;
            transform.LookAt(hitPos);
        }
    }

    public void EquipArm(ArmBase arm)
    {
        bool armEquipped = false;
        if (leftArm == null)
        {
            armEquipped = true;
            arm.Equip(this, shoulderLeft,true);
            leftArm = arm;
            
            bloodSprayerLeft.SetActive(false);
            IngameHud.Instance.UpdateArmInfo(leftArm: true, arm.Ammo, arm.Type.ToString().ToUpper());
        }
        else if (rightArm == null)
        {
            armEquipped = true;
            arm.Equip(this, shoulderRight,false);
            rightArm = arm;
            
            bloodSprayerRight.SetActive(false);
            IngameHud.Instance.UpdateArmInfo(leftArm: false, arm.Ammo, arm.Type.ToString().ToUpper());
        }
        
        if (armEquipped && audioCooldownTimer < 0 && !audioSource.isPlaying)
        {
            AudioClip clip = pickupArmAudioClips[Random.Range(0, pickupArmAudioClips.Length)];
            audioSource.clip = clip;
            audioSource.Play();
        }
    }

    public void RemoveArm(ArmBase arm)
    {
        bool lostArm = false;
        if (leftArm == arm)
        {
            lostArm = true;
            leftArm = null;
            bloodSprayerLeft.SetActive(true);
            IngameHud.Instance.UpdateArmInfo(leftArm: true, 0, "NO ARM!");
        }
        else if (rightArm == arm)
        {
            lostArm = true;
            rightArm = null;
            bloodSprayerRight.SetActive(true);
            IngameHud.Instance.UpdateArmInfo(leftArm: false, 0, "NO ARM!");
        }

        if (lostArm && audioCooldownTimer < 0 && !audioSource.isPlaying)
        {
            AudioClip clip = lostArmAudioClips[Random.Range(0, lostArmAudioClips.Length)];
            audioSource.clip = clip;
            audioSource.Play();
        }
    }

    public void KilledEnemy()
    {
        if (audioCooldownTimer < 0 && !audioSource.isPlaying)
        {
            AudioClip clip = killedEnemyAudioClips[Random.Range(0, killedEnemyAudioClips.Length)];
            audioSource.clip = clip;
            audioSource.Play();
        }
    }

    public float MoveSpeed { get { return moveSpeed; } }
    public bool CanPickupArm { get { return leftArm == null || rightArm == null; } }
}
