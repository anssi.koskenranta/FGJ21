using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{

    [SerializeField] 
    TMP_Text healthTex;

    [SerializeField]
    Image healthBar;

    [SerializeField]
    float animationTime = 1f;

    float currentValue = 1;
    float targetValue = 1;
    float lastValue = 1;
    float currentTime = 0;

    void Start()
    {
        lastValue = 1;
        targetValue = 1;
        currentValue = 1;
        healthBar.fillAmount = currentValue;
    }

    public void UpdateValue(float newValue)
    {
        lastValue = currentValue;
        targetValue = newValue;
        currentTime = 0;
        healthTex.text = Mathf.RoundToInt(newValue * 100).ToString();
    }

    void Update()
    {
        if (currentTime < animationTime)
        {
            currentTime += Time.deltaTime;
        }

        currentValue = Mathf.Lerp(lastValue, targetValue, currentTime/animationTime);
        healthBar.fillAmount = currentValue;
    }
    
}
