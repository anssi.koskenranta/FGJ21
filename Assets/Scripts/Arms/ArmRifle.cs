using UnityEngine;

public class ArmRifle : ArmBase
{
    [SerializeField]
    float fireRate = 30;

    [SerializeField]
    float maxAngle = 5;
    
    float timer = 0;
    
    public override void Shoot()
    {
        isShooting = true;
        //base.Shoot();
    }

    public override void ShootEnd()
    {
        isShooting = false;
        base.ShootEnd();
    }

    void Update()
    {
        if (!isShooting) return;

        timer += Time.deltaTime * fireRate;
        if (timer > 1f)
        {
            timer = 0;

            Bullet bullet = BulletPool.Get();
            bullet.Setup(player != null, ShootPoint.position, ShootPoint.rotation, maxAngle);

            base.Shoot();
        }
    }

    public override ArmType Type { get { return ArmType.Minigun; } }
}
