public class ArmPistol : ArmBase
{
    
    public override void Shoot()
    {
        Bullet bullet = BulletPool.Get();
        bullet.Setup(player != null, ShootPoint.position, ShootPoint.rotation);
        
        base.Shoot();
    }

    public override ArmType Type { get { return ArmType.Pistol; } }
}
