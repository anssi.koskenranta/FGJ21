using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource audioSource;
    GameManager manager;
    public static MusicManager Instance;

    // Start is called before the first frame update

    void Awake()
    {

    }

    void Start()
    {
        manager = GameManager.instance;
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (manager.PlayerHp > 0) return;
        audioSource.pitch = 0.6f;
        

    }
}
