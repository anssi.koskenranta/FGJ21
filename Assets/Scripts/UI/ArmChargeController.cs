using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ArmChargeController : MonoBehaviour
{
    [SerializeField]
    HandUi leftHandUi;
    
    [SerializeField]
    HandUi rightHandUi;

    public void UpdateValue(bool left, int value, string name)
    {
        if (left)
        {
            leftHandUi.SetValue(value, name);
        }
        else
        {
            rightHandUi.SetValue(value, name);
        }
    }

    [System.Serializable]
    class HandUi
    {
        [SerializeField]    
        public Image image;
        
        [SerializeField]
        TextMeshProUGUI weaponName = null;

        [SerializeField]
        TextMeshProUGUI weaponAmmo = null;
        
        [SerializeField]
        Sprite handless = null;

        [SerializeField]
        Sprite handon = null;

        string lastName = "";

        public void SetValue(int value, string name)
        {
            if (lastName != name)
            {
                lastName = name;
                weaponName.text = name;
            }
            
            if (value > 0)
            {
                image.sprite = handon;
                weaponAmmo.text = value.ToString();
                
                if (!weaponAmmo.gameObject.activeSelf)
                    weaponAmmo.gameObject.SetActive(true);
            }
            else
            {
                image.sprite = handless;
                
                if (weaponAmmo.gameObject.activeSelf)
                    weaponAmmo.gameObject.SetActive(false);
            }
        }
    }
    
}
