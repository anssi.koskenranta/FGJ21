﻿using System;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [Serializable]
    class MinMax
    {
        [SerializeField]
        Vector2 min = Vector2.negativeInfinity;

        [SerializeField]
        Vector2 max = Vector2.positiveInfinity;

        public Vector2 Min { get { return min; } }
        public Vector2 Max { get { return max; } }
    }

    [SerializeField]
    Vector3 offset = Vector3.zero;

    [SerializeField, Tooltip("Y = Z axis")]
    MinMax levelLimit = null;
    
    Transform followTarget = null;

    public void Setup(Transform followTarget)
    {
        this.followTarget = followTarget;
    }

    public void Dispose()
    {
        followTarget = null;
    }

    public void _Update()
    {
        Vector3 pos = followTarget.position;

        pos.x = Mathf.Clamp(pos.x, levelLimit.Min.x, levelLimit.Max.x);
        pos.z = Mathf.Clamp(pos.z, levelLimit.Min.y, levelLimit.Max.y);

        pos += offset;

        transform.position = pos;
    }
}
